/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.werapan.databaseproject.component;

import com.werapan.databaseproject.model.Product;
import com.werapan.databaseproject.service.ProductService;
import java.awt.GridLayout;
import java.util.ArrayList;

/**
 *
 * @author MSiRTX
 */
public class ProductListPanel extends javax.swing.JPanel implements BuyProductable {

    private final ProductService productService;
    private final ArrayList<Product> products;
    private ArrayList<BuyProductable> subscribers = new ArrayList<>();

    /**
     * Creates new form ProductListPanel
     */
    public ProductListPanel() {
        initComponents();
        productService = new ProductService();
        products = productService.getProductOderByName();
        int productSize = products.size();
        for (Product p : products) {
            ProductItemPanel pnlProductItem = new ProductItemPanel(p);
            pnlProductItem.addOnBuyProduct(this);
            pnlProductList.add(pnlProductItem);
        }
        pnlProductList.setLayout(new GridLayout((productSize / 3) + ((productSize % 3 != 0) ? 1 : 0), 3, 0, 0));
    }

    public void addOnBuyProduct(BuyProductable subscriber) {
        subscribers.add(subscriber);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        pnlProductList = new javax.swing.JPanel();

        javax.swing.GroupLayout pnlProductListLayout = new javax.swing.GroupLayout(pnlProductList);
        pnlProductList.setLayout(pnlProductListLayout);
        pnlProductListLayout.setHorizontalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 369, Short.MAX_VALUE)
        );
        pnlProductListLayout.setVerticalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 517, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(pnlProductList);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 475, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlProductList;
    // End of variables declaration//GEN-END:variables
    @Override
    public void buy(Product product, int qty) {
        System.out.println("" + product.getName() + " " + qty);
        for (BuyProductable s : subscribers) {
            s.buy(product,qty);
        }
    }
}
